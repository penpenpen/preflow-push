/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package preflowpushalgorithm;

import java.util.Comparator;

/**
 *
 * @author masashi
 */
public class EdgeComparator implements Comparator<MyEdge>{

    @Override
    public int compare(MyEdge e1,MyEdge e2) {
        return e1.getId() < e2.getId() ? -1 : 1;
    }
    
}
