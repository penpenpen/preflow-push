/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preflowpushalgorithm;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import java.util.ArrayList;

/**
 *
 * @author masashi
 */
class MainAlgorithm {
    ArrayList<MyNode> activeNodes;
    public MainAlgorithm(DirectedSparseGraph<MyNode, MyEdge> g,ArrayList<MyNode> listv,ArrayList<MyEdge> liste){
        activeNodes = new ArrayList<>();
        int p=0;
        int d=2*listv.size();
        MyNode acNode,nextNode,preNode;
        
        DistanceLabels dl =new DistanceLabels(listv);
        
        for (MyEdge myEdge : g.getOutEdges(listv.get(0))) {
            myEdge.setFlows(myEdge.getCapa());
            nextNode =g.getOpposite(listv.get(0), myEdge);
            nextNode.addExcess(myEdge.getCapa());
        }
        activeNodes.addAll(listv.get(0).getSucnodes());
        listv.get(0).setD(listv.size());
        
        while (activeNodes.size()>0){
            acNode = activeNodes.get(0);
            for (MyEdge myEdge : g.getInEdges(acNode)){
                preNode = g.getOpposite(acNode, myEdge);
                if(acNode.getD()>preNode.getD()&&myEdge.getFlows()>0){
                    int push =Math.min(myEdge.getFlows(), acNode.getExcess());
                    acNode.addExcess(-push);
                    myEdge.addFlows(-push);
                    preNode.addExcess(push);
                    p = 1;
                    if(preNode ==listv.get(listv.size()-1)||preNode ==listv.get(0)){
                        if(acNode.getExcess()==0){
                            activeNodes.remove(acNode);
                        }
                        break;
                    }else if(activeNodes.contains(preNode)== false){
                        activeNodes.add(g.getOpposite(acNode, myEdge));
                    }
                    if(acNode.getExcess()==0){
                        activeNodes.remove(acNode);
                    }
                    break;
                }
            }
            for (MyEdge myEdge : g.getOutEdges(acNode)) {
                nextNode =g.getOpposite(acNode, myEdge);
                if(acNode.getD()>nextNode.getD()&&myEdge.getExtflows()>0){
                    int push =Math.min(myEdge.getExtflows(), acNode.getExcess());
                    acNode.addExcess(-push);
                    myEdge.addFlows(push);
                    nextNode.addExcess(push);
                    p = 1;
                    if(nextNode ==listv.get(listv.size()-1)||nextNode ==listv.get(0)){
                        if(acNode.getExcess()==0){
                            activeNodes.remove(acNode);
                        }
                        break;
                    }else if(activeNodes.contains(nextNode)== false){
                        activeNodes.add(g.getOpposite(acNode, myEdge));
                    }
                    if(acNode.getExcess()==0){
                        activeNodes.remove(acNode);
                    }
                    break;
                }
            }

            if(p==0){
                for (MyNode myNode : g.getSuccessors(acNode)) {
                    if(myNode.getD()<d && g.findEdge(acNode, myNode).getExtflows()>0){
                        d=myNode.getD()+1;
                    }
                }
                for (MyNode myNode : g.getPredecessors(acNode)) {
                    if(myNode.getD()<d && g.findEdge(myNode,acNode).getFlows()>0){
                        d=myNode.getD()+1;
                    }
                }
                acNode.setD(d);
            }
            p=0;
            d=2*listv.size();
        }
    }
}
