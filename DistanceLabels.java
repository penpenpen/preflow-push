/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preflowpushalgorithm;

import java.util.ArrayList;

/**
 *
 * @author masashi
 */
class DistanceLabels {

    public DistanceLabels(ArrayList<MyNode> listv) {
        MyNode dNode;
        int d;
        for (int i = listv.size(); i >0; i--) {
            dNode =listv.get(i-1);
            d=dNode.getD();
            if(i==listv.size()){
                dNode.setD(0);
            }
            else{
                for (MyNode myNode : dNode.getSucnodes()) {
                    if(dNode.getD()>myNode.getD()+1){
                        dNode.setD(myNode.getD()+1);
                    }
                }
                for (MyNode myNode : dNode.getPrenodes()) {
                    if(dNode.getD()+1<myNode.getD()){
                        myNode.setD(dNode.getD()+1);
                    }
                }
            }
        }
    }
    
}
