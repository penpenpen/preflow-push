/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preflowpushalgorithm;

import java.util.Random;

/**
 *
 * @author masashi
 */
class MyEdge {
    private int capa;
    private int flows;
    private int id;

    public MyEdge(int id) {
        Random rdm = new Random();
        capa = 1+rdm.nextInt(10);
        if(id==0)
            capa =(int)Double.POSITIVE_INFINITY;
        this.id = id;
    }

    @Override
    public String toString() {
        return "edge"+id+":f/c="+flows+"/"+capa;
    }

    public int getExtflows() {
        return capa-flows;
    }

    public int getId() {
        return id;
    }

    public int getCapa() {
        return capa;
    }

    public void setFlows(int flows) {
        this.flows = flows;
    }
    
    public void addFlows(int flows) {
        this.flows = this.flows+flows;
    }

    public int getFlows() {
        return flows;
    }
}
