/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preflowpushalgorithm;

import edu.uci.ics.jung.algorithms.layout.LayoutDecorator;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Paint;
import java.awt.geom.Point2D;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.apache.commons.collections15.Transformer;

/**
 *
 * @author masashi
 */
class ViewGgaph {
    private GridBagLayout gbl;
    private Point2D p;
    private Transformer <MyEdge, Paint> edgePaint = new Transformer<MyEdge, Paint>() {
        @Override
        public Paint transform(MyEdge e) {
            if(e.getFlows()==e.getCapa()){
                return Color.BLUE;
            }
            return Color.BLACK;
        }
    };
    
    public ViewGgaph(Graph<MyNode, MyEdge> g) {
        gbl = new GridBagLayout();
               
        JPanel basePanel = new JPanel(gbl);
        int size = 300;
  
        StaticLayout layout = new StaticLayout(g);
        p = new Point2D.Double(0,0);
        
        layout.setSize(new Dimension(size, size));
        
        for (MyNode myNode : g.getVertices()) {
            p.setLocation(myNode.getP());
            layout.setLocation(myNode, p);
        }
                
        VisualizationViewer vv = new VisualizationViewer(layout);
        vv.setPreferredSize(new Dimension(size*2, size*2));
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<MyNode>());
        vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<MyEdge>());
        vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line());
        vv.getRenderContext().setEdgeDrawPaintTransformer(edgePaint);
        
        DefaultModalGraphMouse gm = new DefaultModalGraphMouse();
        gm.setMode(ModalGraphMouse.Mode.PICKING);
        vv.setGraphMouse(gm);
        
        LayoutDecorator ld = new LayoutDecorator(layout) {
            
        };
        
        basePanel.add(vv);   
        makeFrame(basePanel);
    }
    
    private void makeFrame(JPanel basePanel) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(basePanel);
        frame.pack();
        frame.setVisible(true);
    }
}
