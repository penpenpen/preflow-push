/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preflowpushalgorithm;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author masashi
 */
class MyNode {
    private int id = 0;
    private int d =(int)Double.POSITIVE_INFINITY-1;
    private int excess=0;
    private Point2D p = new Point2D.Double(0, 0);
    private ArrayList<MyNode> prenodes = new ArrayList();
    private ArrayList<MyNode> sucnodes = new ArrayList();

    public MyNode(int id) {
        this.id = id;
    }

    public void setP(Double x,Double y) {
        p.setLocation(x, y);
    }

    public Point2D getP() {
        return p;
    }

    public int getId() {
        return id;
    }

    public int getExcess() {
        return excess;
    }

    public void setExcess(int excess) {
        this.excess = excess;
    }
    
    public void addExcess(int excess) {
        this.excess = this.excess+excess;
    }

    public ArrayList<MyNode> getPrenodes() {
        return prenodes;
    }
    
   public void addPrenodes(Collection<MyNode> colle){
       prenodes.addAll(colle);
    }

    public void setPrenodes(Collection<MyNode> prenodes) {
        this.prenodes.clear();
        this.prenodes.addAll(prenodes);
    }

    public void setSucnodes(Collection<MyNode> sucnodes) {
        this.sucnodes.clear();
        this.sucnodes.addAll(sucnodes);
    }
   
   
   public ArrayList<MyNode> getSucnodes() {
        return sucnodes;
    }
    
   public void addSucnodes(Collection<MyNode> colle){
       sucnodes.addAll(colle);
    }
    
    public void setD(int d) {
        this.d = d;
    }

    public int getD() {
        return d;
    }
    
    @Override
    public String toString() {
        return "node" + id+":d="+d;
    }

}
