/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preflowpushalgorithm;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author masashi
 */
class Model {
    private DirectedSparseGraph<MyNode, MyEdge> g;
    ArrayList<MyNode> listsNodes;
    ArrayList<MyNode> activeNodes;
    ArrayList<MyEdge> listsEdges;
    
    public Model() {
        g = new DirectedSparseGraph<>();
        listsNodes = new ArrayList();
        listsEdges = new ArrayList();
        Point2D p = new Point2D.Double(0, 0);

        for (int i = 0; i < 8; i++) {
            MyNode a = new MyNode(i);
            g.addVertex(a);
            listsNodes.add(a);
        }
        listsNodes.get(0).setP(50.0, 300.0);
        listsNodes.get(1).setP(175.0, 100.0);
        listsNodes.get(2).setP(175.0, 300.0);
        listsNodes.get(3).setP(175.0, 500.0);
        listsNodes.get(4).setP(350.0, 100.0);
        listsNodes.get(5).setP(350.0, 300.0);
        listsNodes.get(6).setP(350.0, 500.0);
        listsNodes.get(7).setP(500.0,300.0);
        
        g.addEdge(new MyEdge(0), listsNodes.get(0),listsNodes.get(1));
        g.addEdge(new MyEdge(1), listsNodes.get(0),listsNodes.get(2));
        g.addEdge(new MyEdge(2), listsNodes.get(0),listsNodes.get(3));
        g.addEdge(new MyEdge(3), listsNodes.get(1),listsNodes.get(2));
        g.addEdge(new MyEdge(4), listsNodes.get(2),listsNodes.get(3));
        g.addEdge(new MyEdge(5), listsNodes.get(1),listsNodes.get(4));
        g.addEdge(new MyEdge(6), listsNodes.get(1),listsNodes.get(5));
        g.addEdge(new MyEdge(7), listsNodes.get(2),listsNodes.get(5));
        g.addEdge(new MyEdge(8), listsNodes.get(3),listsNodes.get(5));
        g.addEdge(new MyEdge(9), listsNodes.get(3),listsNodes.get(6));
        g.addEdge(new MyEdge(10), listsNodes.get(5),listsNodes.get(4));
        g.addEdge(new MyEdge(11), listsNodes.get(6),listsNodes.get(5));
        g.addEdge(new MyEdge(12), listsNodes.get(4),listsNodes.get(7));
        g.addEdge(new MyEdge(13), listsNodes.get(5),listsNodes.get(7));
        g.addEdge(new MyEdge(14), listsNodes.get(6),listsNodes.get(7));
        
        listsEdges.addAll(g.getEdges());
        Collections.sort(listsEdges,new EdgeComparator());
       
        
        for (MyEdge myEdge : listsEdges) {
            System.out.println(myEdge);
        }

        for (MyNode myNode : listsNodes) {
            myNode.setPrenodes(g.getPredecessors(myNode));
            myNode.setSucnodes(g.getSuccessors(myNode));
            System.out.print("Node"+myNode.getId());
            System.out.print(myNode.getPrenodes());
            System.out.println(myNode.getSucnodes());
        }
        
        MainAlgorithm ma = new MainAlgorithm(g,listsNodes,listsEdges);
        
        ViewGgaph vg = new ViewGgaph(g);
    }
    
}
